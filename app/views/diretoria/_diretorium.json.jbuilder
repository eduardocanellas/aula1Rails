json.extract! diretorium, :id, :name, :created_at, :updated_at
json.url diretorium_url(diretorium, format: :json)
