class DiretoriaController < ApplicationController
  before_action :set_diretorium, only: [:show, :edit, :update, :destroy]

  # GET /diretoria
  # GET /diretoria.json
  def index
    @diretoria = Diretorium.all
  end

  # GET /diretoria/1
  # GET /diretoria/1.json
  def show
  end

  # GET /diretoria/new
  def new
    @diretorium = Diretorium.new
  end

  # GET /diretoria/1/edit
  def edit
  end

  # POST /diretoria
  # POST /diretoria.json
  def create
    @diretorium = Diretorium.new(diretorium_params)

    respond_to do |format|
      if @diretorium.save
        format.html { redirect_to @diretorium, notice: 'Diretorium was successfully created.' }
        format.json { render :show, status: :created, location: @diretorium }
      else
        format.html { render :new }
        format.json { render json: @diretorium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /diretoria/1
  # PATCH/PUT /diretoria/1.json
  def update
    respond_to do |format|
      if @diretorium.update(diretorium_params)
        format.html { redirect_to @diretorium, notice: 'Diretorium was successfully updated.' }
        format.json { render :show, status: :ok, location: @diretorium }
      else
        format.html { render :edit }
        format.json { render json: @diretorium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /diretoria/1
  # DELETE /diretoria/1.json
  def destroy
    @diretorium.destroy
    respond_to do |format|
      format.html { redirect_to diretoria_url, notice: 'Diretorium was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_diretorium
      @diretorium = Diretorium.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def diretorium_params
      params.require(:diretorium).permit(:name)
    end
end
