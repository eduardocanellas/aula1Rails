class Post < ApplicationRecord
  belongs_to :Users


  validates :title, length: { in: 6..140 }
  validates :content, length: { in: 6..140 }
  validates :Users, presence: true

end
