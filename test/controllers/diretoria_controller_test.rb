require 'test_helper'

class DiretoriaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @diretorium = diretoria(:one)
  end

  test "should get index" do
    get diretoria_url
    assert_response :success
  end

  test "should get new" do
    get new_diretorium_url
    assert_response :success
  end

  test "should create diretorium" do
    assert_difference('Diretorium.count') do
      post diretoria_url, params: { diretorium: { name: @diretorium.name } }
    end

    assert_redirected_to diretorium_url(Diretorium.last)
  end

  test "should show diretorium" do
    get diretorium_url(@diretorium)
    assert_response :success
  end

  test "should get edit" do
    get edit_diretorium_url(@diretorium)
    assert_response :success
  end

  test "should update diretorium" do
    patch diretorium_url(@diretorium), params: { diretorium: { name: @diretorium.name } }
    assert_redirected_to diretorium_url(@diretorium)
  end

  test "should destroy diretorium" do
    assert_difference('Diretorium.count', -1) do
      delete diretorium_url(@diretorium)
    end

    assert_redirected_to diretoria_url
  end
end
