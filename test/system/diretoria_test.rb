require "application_system_test_case"

class DiretoriaTest < ApplicationSystemTestCase
  setup do
    @diretorium = diretoria(:one)
  end

  test "visiting the index" do
    visit diretoria_url
    assert_selector "h1", text: "Diretoria"
  end

  test "creating a Diretorium" do
    visit diretoria_url
    click_on "New Diretorium"

    fill_in "Name", with: @diretorium.name
    click_on "Create Diretorium"

    assert_text "Diretorium was successfully created"
    click_on "Back"
  end

  test "updating a Diretorium" do
    visit diretoria_url
    click_on "Edit", match: :first

    fill_in "Name", with: @diretorium.name
    click_on "Update Diretorium"

    assert_text "Diretorium was successfully updated"
    click_on "Back"
  end

  test "destroying a Diretorium" do
    visit diretoria_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Diretorium was successfully destroyed"
  end
end
